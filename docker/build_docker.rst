# remove all images
docker system prune -a

# rebuild images
docker build -t revesansparole/glabci:latest glabci

# check last image
docker run -it revesansparole/glabci bash

# push image
docker login

docker push revesansparole/glabci:latest


# multiplatform
https://nielscautaerts.xyz/making-dockerfiles-architecture-independent.html

docker buildx build --platform linux/amd64,linux/arm64 -t revesansparole/glabci --push glabci
