====================================================
Welcome to {{ base.pkg_full_name }}'s documentation!
====================================================

.. toctree::
    :caption: Contents
    :maxdepth: 2

    introduction

.. toctree::
    :caption: Annexe
    :maxdepth: 1

    gitlab home <{{ gitlab.url }}>
    authors
    badges <badges/listing>
