============
Installation
============

.. {# pkglts, glabpkg_dev

This package is designed for editable install only::

    $ activate myenv
    (myenv) $ pip install -e {{ base.pkgname }}

.. #}
