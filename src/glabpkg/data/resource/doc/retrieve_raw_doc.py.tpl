# {# pkglts, gladata
import shutil
from pathlib import Path

tgt_dir = Path("{{ glabpkg_dev.user_doc }}")
if tgt_dir.exists():
    shutil.rmtree(tgt_dir, ignore_errors=True)

src_dir = Path("../raw/doc")

shutil.copytree(src_dir, tgt_dir)

src_dir = Path("../raw/biblio")

shutil.copytree(src_dir, tgt_dir / "biblio")

# #}
