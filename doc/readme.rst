Overview
========

.. {# pkglts, glabpkg_dev

.. image:: https://revesansparole.gitlab.io/glabpkg/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/glabpkg/5.0.0/


.. image:: https://revesansparole.gitlab.io/glabpkg/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/glabpkg


.. image:: https://revesansparole.gitlab.io/glabpkg/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://revesansparole.gitlab.io/glabpkg/


.. image:: https://badge.fury.io/py/glabpkg.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/glabpkg




main: |main_build|_ |main_coverage|_

.. |main_build| image:: https://gitlab.com/revesansparole/glabpkg/badges/main/pipeline.svg
.. _main_build: https://gitlab.com/revesansparole/glabpkg/commits/main

.. |main_coverage| image:: https://gitlab.com/revesansparole/glabpkg/badges/main/coverage.svg
.. _main_coverage: https://gitlab.com/revesansparole/glabpkg/commits/main

prod: |prod_build|_ |prod_coverage|_

.. |prod_build| image:: https://gitlab.com/revesansparole/glabpkg/badges/prod/pipeline.svg
.. _prod_build: https://gitlab.com/revesansparole/glabpkg/commits/prod

.. |prod_coverage| image:: https://gitlab.com/revesansparole/glabpkg/badges/prod/coverage.svg
.. _prod_coverage: https://gitlab.com/revesansparole/glabpkg/commits/prod
.. #}

template for python packages hosted on gitlab
